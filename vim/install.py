#!/usr/bin/env python

# -*- coding: utf8 -*-

from __future__ import print_function
import yaml
import os
import sys
import argparse
import subprocess as sb

# external update script required
PLUGINS_FILE_DOWNLOAD = (
    'wget '
    'https://bitbucket.org/Dalamar/dot_files/raw/'
    'master/vim/plugins.yaml'
)
PLUGINS_FILE_REMOVE = 'rm -f plugins.yaml'
VIMRC_PATH = os.path.expanduser(os.path.join('~', '.vimrc'))
VUNDLE_INSTALL_COMMAND = (
    'git clone https://github.com/gmarik/Vundle.vim.git '
    + os.path.expanduser(os.path.join('~', '.vim', 'bundle', 'Vundle.vim'))
)
VUNDLE_INSTALL_PLUGINS_COMMAND = 'vim -e +PluginInstall +qall'


def safe_shell_call(command, ignore_errors=False):
    print("Executing command %s" % command)
    command_handler = sb.Popen(command.split(), stderr=sb.PIPE)
    _, stderr = command_handler.communicate()
    if command_handler.returncode != 0 and not ignore_errors:
        print ("Error while executing '''%s''':" % command)
        print (stderr, end='')
        sys.exit(command_handler.returncode)


def check_vimrc_exists():
    if not os.path.exists(VIMRC_PATH):
        return

    while True:
        answer = raw_input(
            "Warning: %s file exists. Override it? (y/n) " % VIMRC_PATH
        )
        if answer == 'y':
            return
        elif answer == 'n':
            print ("Denied to override %s" % VIMRC_PATH, file=sys.stderr)
            sys.exit(1)


def choose_plugins(all_plugins, parameters):
    plugins = {}
    for name, plugin in all_plugins.items():
        if set([parameters.install_type, 'default']) & set(plugin['subset']):
            plugins[name] = plugin

    return plugins


def check_dependicies(plugins):
    pass # TODO FIXME


def formatted_headers(headers_name):
    spaced_name = ' ' + headers_name + ' '
    return '" {0:=^77}\n" {1:=^77}\n" {0:=^77}\n\n'.format('', spaced_name)


def write_plugins_includes(fh, plugins):
    for plugin in plugins.values():
        if 'path' in plugin:
            if 'docstring' in plugin:
                fh.write('" ' + plugin['docstring'] + '\n')
            fh.write("Plugin '" + plugin['path'] + "'\n")

    fh.write('\n')

def write_plugins_settings(fh, plugins):
    for plugin_name, plugin in plugins.items():
        if not 'settings' in plugin:
            continue
        fh.write(formatted_headers(plugin_name))
        fh.write(plugin['settings'] + '\n')


def write_plugins_keymappings(fh, plugins):
    keymaps = (plugin['keymap'] for plugin in plugins if 'keymap' in plugin)
    for keymap in keymaps:
        fh.write(keymap + '\n\n')


def write_plugins_syntaxes(fh, plugins):
    plugins_with_syntax = [plugin for plugin in plugins.values()
                           if 'syntax' in plugin]
    for plugin in plugins_with_syntax:
        if 'syntax_docstring' in plugin:
            fh.write('" ' + plugin['syntax_docstring'] + '\n')
        fh.write(plugin['syntax'] + '\n')
    if plugins_with_syntax:
        fh.write('\n')

def build_plugins(plugins):
    pass # TODO FIXME

def vimrc_postinstall():
    os.mkdir(os.path.expanduser(os.path.join('~', '.vim', 'undo')))
    os.mkdir(os.path.expanduser(os.path.join('~', '.vim', 'swap')))


def install_vimrs(parameters):
    if not parameters.silent:
        check_vimrc_exists()

    # safe_shell_call(DOWNLOAD_PLUGINS_JSON)
    vimrc_template = yaml.load(open('plugins.yaml'))
    plugins = choose_plugins(vimrc_template['plugins'], parameters)
    check_dependicies(plugins)

    vimrc_file = open(VIMRC_PATH, 'w')

    vimrc_file.write(formatted_headers('Vundle Settings'))
    vimrc_file.write(vimrc_template['vundle_begin'] + '\n')
    write_plugins_includes(vimrc_file, plugins)
    vimrc_file.write(vimrc_template['vundle_end'] + '\n')

    write_plugins_settings(vimrc_file, plugins)

    vimrc_file.write(formatted_headers("Custom Functions"))
    vimrc_file.write(vimrc_template['custom_functions'] + '\n')

    vimrc_file.write(formatted_headers('Key Mappings'))
    vimrc_file.write(vimrc_template['keymaps'] + '\n')
    write_plugins_keymappings(vimrc_file, plugins)

    vimrc_file.write(formatted_headers('General Settings'))
    write_plugins_syntaxes(vimrc_file, plugins)
    vimrc_file.write(vimrc_template['general_settings'].encode('utf8'))
    vimrc_file.close()

    print("%s generated, trying to install Vundle" % VIMRC_PATH)
    safe_shell_call(VUNDLE_INSTALL_COMMAND)

    print("Vundle installed, trying to install other plugins")
    safe_shell_call(VUNDLE_INSTALL_PLUGINS_COMMAND, ignore_errors=True)
    build_plugins(plugins)
    vimrc_postinstall()

    print("Installed :)")


def _create_parser():
    parser_ = argparse.ArgumentParser(description='Install vim files')
    parser_.add_argument('install_type', type=str,
                        choices=['adhoc', 'dev'])
    parser_.add_argument('--silent', '-s', action='store_true')

    return parser_


if __name__ == '__main__':
    parser = _create_parser()
    args = parser.parse_args()
    safe_shell_call(PLUGINS_FILE_DOWNLOAD)
    install_vimrs(args)
    safe_shell_call(PLUGINS_FILE_REMOVE)
